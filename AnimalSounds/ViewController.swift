//
//  ViewController.swift
//  AnimalSounds
//
//  Created by Sampada Sakpal on 9/8/20.
//  Copyright © 2020 Sampada Sakpal. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    let meowSound = SimpleSound(named: "meow");
    let woofSound = SimpleSound(named: "woof");
    let mooSound = SimpleSound(named: "moo");
    @IBAction func cowButtonTapped(_ sender: Any) {
        animalSoundLabel.text = "Moo!"
        mooSound.play()
    }
    @IBAction func dogButtonTapped(_ sender: Any) {
         animalSoundLabel.text = "Woof!"
        woofSound.play()
    }
    @IBAction func catButtonTapped(_ sender: Any) {
        animalSoundLabel.text = "Meow!"
        meowSound.play()
    }
    @IBOutlet weak var animalSoundLabel: UILabel!
    
}

